
import java.util.LinkedList;

public class Geometry implements IGeometry {

	private double[][] vertices;
	private int[][] face;
	public Matrix matrix;
	public Material material = new Material();
	
	private double[][][] lights={
			{{1,1,1},{1,1,1}}
			};
	
	public void setLights(double [][][] lights){
		this.lights = lights;
	}
	
	public double[][] getVertices(){
		return  this.vertices;
	}
	
	public int[][] getFace(){
		return this.face;
	}
	
	private LinkedList<Geometry> childList = new LinkedList<Geometry>();
	

	public static final int NOT_INIT = -1;
	public static final int CUBE=0;
	public static final int OCTAHEDRON = 1;
	public static final int GLOBE = 2;
	public static final int SPHERE = 3;
	public static final int CYLINDER = 4;
	public static final int TORUS = 5;
	
	private int x = 0;
	private int y = 0;
	private int z = 0;
	
	public void setPosition(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public int getPositionX(){
		return this.x;
	}
	public int getPositionY(){
		return this.y;
	}
	public int getPositionZ(){
		return this.z;
	}
	
	double ax,ay,az,bx,by,bz = 0;
	double tmpSum = 0;
	public void refreshSurfaceNormal(){
		for(double[] v: vertices ){
			v[3] = 0;
			v[4] = 0;
			v[5] = 0;
		}
		
		for(int[] oneFaceIndex : face){
		
			ax = vertices[oneFaceIndex[0]][0] - vertices[oneFaceIndex[oneFaceIndex.length-1]][0];
			ay = vertices[oneFaceIndex[0]][1] - vertices[oneFaceIndex[oneFaceIndex.length-1]][1];
			az = vertices[oneFaceIndex[0]][2] - vertices[oneFaceIndex[oneFaceIndex.length-1]][2];
			
			bx = vertices[oneFaceIndex[1]][0] - vertices[oneFaceIndex[0]][0];
			by = vertices[oneFaceIndex[1]][1] - vertices[oneFaceIndex[0]][1];
			bz = vertices[oneFaceIndex[1]][2] - vertices[oneFaceIndex[0]][2];
			
			for(int index: oneFaceIndex){
				vertices[index][3] += ay*bz - az*by;
				vertices[index][4] += az*bx - ax*bz;
				vertices[index][5] += ax*by - ay*bx;
			}
			

			for(int i=1;i<oneFaceIndex.length-2;i++){
//				vertices[oneFaceIndex[i]][3] +=  
				ax = vertices[oneFaceIndex[i]][0] - vertices[oneFaceIndex[i-1]][0];
				ay = vertices[oneFaceIndex[i]][1] - vertices[oneFaceIndex[i-1]][1];
				az = vertices[oneFaceIndex[i]][2] - vertices[oneFaceIndex[i-1]][2];
				
				bx = vertices[oneFaceIndex[i+1]][0] - vertices[oneFaceIndex[i]][0];
				by = vertices[oneFaceIndex[i+1]][1] - vertices[oneFaceIndex[i]][1];
				bz = vertices[oneFaceIndex[i+1]][2] - vertices[oneFaceIndex[i]][2];
				
//				vertices[oneFaceIndex[i]][3] += ay*bz - az*by;
//				vertices[oneFaceIndex[i]][4] += az*bx - ax*bz;
//				vertices[oneFaceIndex[i]][5] += ax*by - ay*bx;
				for(int index: oneFaceIndex){
					vertices[index][3] += ay*bz - az*by;
					vertices[index][4] += az*bx - ax*bz;
					vertices[index][5] += ax*by - ay*bx;
				}
			}
		}
		
		for(double[] v: vertices ){
			tmpSum = v[3]*v[3]+ v[4]* v[4] + v[5]*v[5];
			tmpSum = Math.sqrt(tmpSum);
			v[3] /= tmpSum;
			v[4] /= tmpSum;
			v[5] /= tmpSum;
//			System.out.print(v[3]+" "+v[4]+" "+v[5] +" " + tmpSum);
//			System.out.println();
		}
	}
	
	public void transform(){
		for(double[] src: vertices)
		this.matrix.transform(src.clone(), src);
		this.refreshSurfaceNormal();
	}

	
	
	double tmpX = 0;
	double tmpY = 0;
	double tmpZ = 0;
	public void rotateX(double radians){
		this.rotateXAux(this,radians);
	}
	private void rotateXAux(Geometry geoNode, double radians){
		geoNode.matrix.rotateX(radians);
		
		for(Geometry child : geoNode.childList){
			tmpX = child.matrix.getCenterX();
			tmpY = child.matrix.getCenterY();
			tmpZ = child.matrix.getCenterZ();
			child.matrix.setCenter(
					geoNode.matrix.getCenterX() - (child.x - geoNode.x), //- child.matrix.getCenterX(), 
					geoNode.matrix.getCenterY() - (child.y - geoNode.y), //- child.matrix.getCenterY(), 
					geoNode.matrix.getCenterZ() - (child.z - geoNode.z)  //- child.matrix.getCenterZ()
					);
//			child.matrix.rotateX(radians);	
			this.rotateXAux(child, radians);
			child.matrix.setCenter(
					tmpX, 
					tmpY, 
					tmpZ
					);
		}
	}
	public void rotateY(double radians){
		this.rotateYAux(this,radians);
	}
	private void rotateYAux(Geometry geoNode, double radians){
		geoNode.matrix.rotateY(radians);
		
//		double tmpX = 0;
//		double tmpY = 0;
//		double tmpZ = 0;
		for(Geometry child : geoNode.childList){
			tmpX = child.matrix.getCenterX();
			tmpY = child.matrix.getCenterY();
			tmpZ = child.matrix.getCenterZ();
			child.matrix.setCenter(
					geoNode.matrix.getCenterX() - (child.x - geoNode.x), 
					geoNode.matrix.getCenterY() - (child.y - geoNode.y), 
					geoNode.matrix.getCenterZ() - (child.z - geoNode.z)
					);
//			child.matrix.rotateY(radians);		
			this.rotateYAux(child, radians);
			child.matrix.setCenter(
					tmpX, 
					tmpY, 
					tmpZ
					);
		}
	}
	public void rotateZ(double radians){
		this.rotateZAux(this,radians);
	}
	private void rotateZAux(Geometry geoNode, double radians){
		geoNode.matrix.rotateZ(radians);
//		double tmpX = 0;
//		double tmpY = 0;
//		double tmpZ = 0;
		for(Geometry child : geoNode.childList){
			tmpX = child.matrix.getCenterX();
			tmpY = child.matrix.getCenterY();
			tmpZ = child.matrix.getCenterZ();
			child.matrix.setCenter(
					geoNode.matrix.getCenterX() - (child.x - geoNode.x), 
					geoNode.matrix.getCenterY() - (child.y - geoNode.y), 
					geoNode.matrix.getCenterZ() - (child.z - geoNode.z)
					);
//			child.matrix.rotateZ(radians);		
			this.rotateZAux(child, radians);
			child.matrix.setCenter(
					tmpX, 
					tmpY, 
					tmpZ
					);
		}
	}
	
	public void identity(){
		this.indentityAux(this);
	}
	private void indentityAux(Geometry geoNode){
		geoNode.matrix.identity();
		for(Geometry child : geoNode.childList){
			child.matrix.identity();
		}
	}
	
	public void scale(double s){
		this.scaleAux(this,s);
	}
	private void scaleAux(Geometry geoNode,double s){
		geoNode.matrix.scale(s);
		for(Geometry child : geoNode.childList){
			child.matrix.scale(s);
		}
	}
	
	public void scale(double x, double y, double z){
		this.scaleAux(this,x,y,z);
	}
	private void scaleAux(Geometry geoNode,double x, double y, double z){
		geoNode.matrix.scale(x,y,z);
		for(Geometry child : geoNode.childList){
			child.matrix.scale(x,y,z);
		}
	}
	
	@Override
	public void add(Geometry child) {
		
		this.childList.add(child);
		
	}


	@Override
	public Geometry getChild(int i) {
		return childList.get(i);
	}


	@Override
	public Matrix getMatrix() {
		return matrix;
	}


	@Override
	public int getNumChildren() {
		return childList.size();
	}


	@Override
	public void remove(Geometry child) {
		childList.remove(child);
	}
	

	private int type = Geometry.NOT_INIT;
	
	public int getType(){
		return this.type;
	}
	
	
	public static Geometry GeometryFactory(int TYPE){

		switch(TYPE){
		case Geometry.CUBE:
			return Geometry.CubeFactory();
		case Geometry.OCTAHEDRON:
			return Geometry.OctahedronFactory();
		case Geometry.GLOBE:
			return Geometry.GLobeFactory(25);
		case Geometry.CYLINDER:
			return Geometry.CylinderFactory(25);
		case Geometry.TORUS:
			return Geometry.TorusFactory(25, 1.0, 0.2);
		case Geometry.SPHERE:
			return Geometry.SphereFactory(25, Math.PI/4, Math.PI/4);
		default:
			return null;
		}	

	}

	public static Geometry CubeFactory(){
		Geometry rtn = new Geometry();
		rtn.type = Geometry.CUBE;
		
//		rtn.vertices = 
//		new double[][]
//				{
//		{0,1,0},{1,1,0},{1,0,0},{0,0,0},
//		{1,1,0},{1,1,1},{1,0,1},{1,0,0},
//		{1,1,1},{0,1,1},{0,0,1},{1,0,1},
//		{0,1,1},{0,1,0},{0,0,0},{0,0,1},
//		{0,0,0},{1,0,0},{1,0,1},{0,0,1},
//		{0,1,1},{1,1,1},{1,1,0},{0,1,0}
//				};
				
		rtn.vertices = 
		new double[][]
				{
		{0,1,0,0,0,-1},{1,1,0,0,0,-1},{1,0,0,0,0,-1},{0,0,0,0,0,-1},
		{1,1,0,1,0,0},{1,1,1,1,0,0},{1,0,1,1,0,0},{1,0,0,1,0,0},
		{1,1,1,0,0,1},{0,1,1,0,0,1},{0,0,1,0,0,1},{1,0,1,0,0,1},
		{0,1,1,-1,0,0},{0,1,0,-1,0,0},{0,0,0,-1,0,0},{0,0,1,-1,0,0},
		{0,0,0,0,-1,0},{1,0,0,0,-1,0},{1,0,1,0,-1,0},{0,0,1,0,-1,0},
		{0,1,1,0,1,0},{1,1,1,0,1,0},{1,1,0,0,1,0},{0,1,0,0,1,0}
				};
		

		
//		rtn.vertices = 
//				new double[][]
//						{
//				{-0.5,0.5,-0.5},{0.5,0.5,-0.5},{0.5,-0.5,-0.5},{-0.5,-0.5,-0.5},
//				{0.5,0.5,-0.5},{0.5,0.5,0.5},{0.5,-0.5,0.5},{0.5,-0.5,-0.5},
//				{0.5,0.5,0.5},{-0.5,0.5,0.5},{-0.5,-0.5,0.5},{0.5,-0.5,0.5},
//				{-0.5,0.5,0.5},{-0.5,0.5,-0.5},{-0.5,-0.5,-0.5},{-0.5,-0.5,0.5},
//				{-0.5,-0.5,-0.5},{0.5,-0.5,-0.5},{0.5,-0.5,0.5},{-0.5,-0.5,0.5},
//				{-0.5,0.5,0.5},{0.5,0.5,0.5},{0.5,0.5,-0.5},{-0.5,0.5,-0.5}
//						};
		rtn.face = new int[][]{
				{0,1,2,3},
				{4,5,6,7},
				{8,9,10,11},
				{12,13,14,15},
				{16,17,18,19},
				{20,21,22,23}				
		};


		rtn.matrix = new Matrix(4,4);
		rtn.matrix.identity();
		return rtn;
	}

	public static Geometry OctahedronFactory(){
		Geometry rtn = new Geometry();
		rtn.type = Geometry.OCTAHEDRON;
//		rtn.vertices = 
//				new double[][]
//						{
//				{0,0,-1},{0,-1,0},{-1,0,0},
//				{1,0,0},{0,-1,0},{0,0,-1},
//				{-1,0,0},{0,1,0},{0,0,-1},
//				{0,0,-1},{0,1,0},{1,0,0},
//				{-1,0,0},{0,-1,0},{0,0,1},
//				{0,0,1},{0,-1,0},{1,0,0},
//				{0,0,1},{0,1,0},{-1,0,0},
//				{1,0,0},{0,1,0},{0,0,1}
//						};
		rtn.vertices = 
				new double[][]
						{
				{0,0,-1,0,0,0},{0,-1,0,0,0,0},{-1,0,0,0,0,0},
				{1,0,0,0,0,0},{0,-1,0,0,0,0},{0,0,-1,0,0,0},
				{-1,0,0,0,0,0},{0,1,0,0,0,0},{0,0,-1,0,0,0},
				{0,0,-1,0,0,0},{0,1,0,0,0,0},{1,0,0,0,0,0},
				{-1,0,0,0,0,0},{0,-1,0,0,0,0},{0,0,1,0,0,0},
				{0,0,1,0,0,0},{0,-1,0,0,0,0},{1,0,0,0,0,0},
				{0,0,1,0,0,0},{0,1,0,0,0,0},{-1,0,0,0,0,0},
				{1,0,0,0,0,0},{0,1,0,0,0,0},{0,0,1,0,0,0}
						};
		rtn.face = new int[][]{
				{0,1,2},
				{3,4,5},
				{6,7,8},
				{9,10,11},
				{12,13,14},
				{15,16,17},
				{18,19,20},
				{21,22,23}
		};
		rtn.matrix = new Matrix(4,4);
		rtn.matrix.identity();
		return rtn;
	}

	public static Geometry CylinderFactory(int smoth){
		Geometry rtn = new Geometry();
		rtn.type = Geometry.CYLINDER;
		int M = smoth;
		int N = smoth;
		double u = 0;
		double v = 0;
		double rv = 0;
		rtn.vertices = new double[(M+1)*(N+1)][6];
		int index = 0;
		for(int m=0;m<=M;m++){
			for(int n=0;n<=N;n++){
				index = m+(M+1)*n;
				u = 0+2*Math.PI/smoth*m;
				v = 1.0/smoth*n;
				rv = Math.abs(v-0.0) <0.01 || Math.abs(v-1.0)<0.01 ? 0: 1;
				rtn.vertices[index][0] = Math.cos(u)*rv;
				rtn.vertices[index][1] = Math.sin(u)*rv;
				rtn.vertices[index][2] = (v-0.5)<0.001 ? -1: 1;
			}
		}
		rtn.face = new int[M*N][4];
		for(int m=0;m<M;m++){
			for(int n=0;n<N;n++){
				index = m+(M)*n;
				rtn.face[index][0] = m+(M+1)*n;
				rtn.face[index][1] = m+1+(M+1)*n;
				rtn.face[index][2] = m+1+(M+1)*(n+1);
				rtn.face[index][3] = m+(M+1)*(n+1);
			}
		}

		rtn.matrix = new Matrix(4,4);
		rtn.matrix.identity();
		return rtn;
	}

	public static Geometry SphereFactory(int smoth,double U, double V){
		Geometry rtn = new Geometry();
		rtn.type = Geometry.SPHERE;
		int M = smoth;
		int N = smoth;
		double u = 0;
		double v = 0;
		rtn.vertices = new double[(M+1)*(N+1)][6];
		int index = 0;
		for(int m=0;m<=M;m++){
			for(int n=0;n<=N;n++){
				index = m+(M+1)*n;
				u = 0+2*U/smoth*m;
				v = -V/2 + V/smoth*n;
				rtn.vertices[index][0] = Math.cos(u)*Math.cos(v);
				rtn.vertices[index][1] = Math.sin(u)*Math.cos(v);
				rtn.vertices[index][2] = Math.sin(v);
			}
		}
		rtn.face = new int[M*N][4];
		for(int m=0;m<M;m++){
			for(int n=0;n<N;n++){
				index = m+(M)*n;
				rtn.face[index][0] = m+(M+1)*n;
				rtn.face[index][1] = m+1+(M+1)*n;
				rtn.face[index][2] = m+1+(M+1)*(n+1);
				rtn.face[index][3] = m+(M+1)*(n+1);
			}
		}

		rtn.matrix = new Matrix(4,4);
		rtn.matrix.identity();
		return rtn;
	}
	public static Geometry GLobeFactory(int smoth){
		Geometry rtn = new Geometry();
		rtn.type = Geometry.GLOBE;
		int M = smoth;
		int N = smoth;
		double u = 0;
		double v = 0;
		rtn.vertices = new double[(M+1)*(N+1)][6];
		int index = 0;
		for(int m=0;m<=M;m++){
			for(int n=0;n<=N;n++){
				index = m+(M+1)*n;
				u = 0+2*Math.PI/smoth*m;
				v = -Math.PI/2 + Math.PI/smoth*n;
				rtn.vertices[index][0] = Math.cos(u)*Math.cos(v);
				rtn.vertices[index][1] = Math.sin(u)*Math.cos(v);
				rtn.vertices[index][2] = Math.sin(v);
			}
		}
		rtn.face = new int[M*N][4];
		for(int m=0;m<M;m++){
			for(int n=0;n<N;n++){
				index = m+(M)*n;
				rtn.face[index][0] = m+(M+1)*n;
				rtn.face[index][1] = m+1+(M+1)*n;
				rtn.face[index][2] = m+1+(M+1)*(n+1);
				rtn.face[index][3] = m+(M+1)*(n+1);
			}
		}

		rtn.matrix = new Matrix(4,4);
		rtn.matrix.identity();
		return rtn;
	}

	/**
	 * 
	 * @param smoth
	 * @param R 0<R<=1
	 * @param r 0<=r<R<=1
	 * @return
	 */
	public static Geometry TorusFactory(int smoth, double R, double r){
		Geometry rtn = new Geometry();
		rtn.type = Geometry.TORUS;
		int M = smoth;
		int N = smoth;
		double u = 0;
		double v = 0;
		rtn.vertices = new double[(M+1)*(N+1)][6];
		int index = 0;
		for(int m=0;m<=M;m++){
			for(int n=0;n<=N;n++){
				index = m+(M+1)*n;
				u = 0+2*Math.PI/smoth*m;
				v = 0+2*Math.PI/smoth*n;
				rtn.vertices[index][0] = Math.cos(u)*(R+r*Math.cos(v));
				rtn.vertices[index][1] = Math.sin(u)*(R+r*Math.cos(v));
				rtn.vertices[index][2] = r*Math.sin(v);
			}
		}
		rtn.face = new int[M*N][4];
		for(int m=0;m<M;m++){
			for(int n=0;n<N;n++){
				index = m+(M)*n;
				rtn.face[index][0] = m+(M+1)*n;
				rtn.face[index][1] = m+1+(M+1)*n;
				rtn.face[index][2] = m+1+(M+1)*(n+1);
				rtn.face[index][3] = m+(M+1)*(n+1);
			}
		}

		rtn.matrix = new Matrix(4,4);
		rtn.matrix.identity();
		return rtn;
	}

	

	private double[] vector(double[] A, double[] B){
		double[] rtn = A.clone();
		for(int i=0;i<rtn.length;i++){
			rtn[i] = A[i] - B[i];
		}
		return rtn;
	}

//	private double[] crossProduct(double[] A,double[] B){
//		return new double[]{
//				A[1]*B[2] - A[2]*B[1],
//				A[2]*B[0] - A[0]*B[2],
//				A[0]*B[1] - A[1]*B[0]
//		};
//	}
	



	
	
	/**
	 * @param args
	 */
	 public static void main(String[] args) {
		Geometry ge = Geometry.GeometryFactory(Geometry.CUBE);
		for(double[] p : ge.vertices)
			for(double q: p){
				System.out.print(q);
			}
	 }

}
