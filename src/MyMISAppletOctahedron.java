import java.awt.Color;
import java.util.Arrays;


/*<pre>
   This is a simple example to show how to use the MISApplet
   to make your own pixel-by-pixel framebuffer.

   Two methods you can override from the MISApplet base
   class are initFrame and setPixel.
 */

public class MyMISAppletOctahedron extends MISApplet {
	
	private static int WIDTH = 400;
	private static int HEIGHT = 400;
	private static int FOCAL = 10;

	//----- THESE TWO METHODS OVERRIDE METHODS IN THE BASE CLASS

	double t = 0;
	private Geometry cube = null;
//	private int CUBE = 1;
//	private int OCTAHEDRON = 2;
//	private int SPHERE = 3;
//	private int TORUS = 4;
	
	private double[][][] lights={
//			{ { 0.0, 1.0, 0.0}, {0.8, 0.8, 0.8} },
//		      { {0.0,-1.0,0.0}, {1.0, 1.0, 1.0} },
//		      { {1.0,1.0,1.0}, {1.0, 1.0, 1.0} },
//		      { {0.58,0.58,0.58}, {1.0, 1.0, 1.0} },
		      
//		      { { 0.0, 1.0, 0.0}, {1.0, 1.0, 1.0} },
//		      { { 0.0, -1.0, 0.0}, {1.0, 1.0, 1.0} },
//		      { {-1.0,-1.0,-1.0}, {1.0, 1.0, 1.0} },
		      
		      { { 1.0, 1.0, 1.0}, {0.8, 0.8, 0.8} },
		      { {-1.0,-1.0,-1.0}, {0.8, 0.8, 0.8} },
			};
	
	public void setLights(double [][][] lights){
		this.lights = lights;
	}
	
	@Override
	public void init(){
		super.init();
		
	}

	public void initFrame(double time) { // INITIALIZE ONE ANIMATION FRAME

		//       REPLACE THIS CODE WITH YOUR OWN TIME-DEPENDENT COMPUTATIONS, IF ANY.

		t = 10 * time;
//		this.setBackground(Color.WHITE);
		

	}

	int rgbBak[];
	double centerX = 200;
	double centerY = 200;
	double  cwidth= 10;
	double space = 30;
	double r=0;
	double R =0;
	public void setPixel(int x, int y, int rgb[]) { // SET ONE PIXEL'S COLOR
		if(x>=0 && x<W && y>=0 && y<H){
			pix[x+y*W] = pack(rgb[0],rgb[1],rgb[2]);
		}
		
	}
	


	
	private int[] rgb_WHITE = {255,255,255};
	
	double[] A = {0.2,0.0,0.0};
	double[] D = {0.8,0.0,0.0};
	double[] S = {1.0,1.0,1.0};
	double   p = 10;
	
	@Override
	public void computeImage(double time){
		initFrame(time); 
		for(int i=0;i<W;i++){
			for(int j=0;j<H;j++)
				setPixel(i,j,rgb_WHITE);
//			setPixel(i,j,rgb_BLACK);
		}
		
//		cube = Geometry.CubeFactory();
		
		cube = Geometry.OctahedronFactory();
//		cube = Geometry.SphereFactory(30, 1.4, 1.3);
//		cube = Geometry.TorusFactory(30, 1, 0.2);
//		cube = Geometry.GLobeFactory(30);
		
		cube.material.setAmbientColor(A);
		cube.material.setDiffuseColor(D);
		cube.material.setSpecularColor(S);
		cube.material.setSpecularPower(p);
		
		cube.identity();
		cube.scale(150);
		cube.rotateX(t/20);
		cube.rotateY(0.3+t/20);
		cube.rotateZ(0.3+t/20);
		cube.matrix.translate(200, 200, 0);
//		cube.refreshSurfaceNormal();
		cube.transform();

		
		
		drawGeometry(cube,rgb_WHITE);

	} 
	
	private double[][] oneFaceVertices = null;;
	private double[][] geoVertices = null;
	private double[][] dst = null;;
	
	public void drawGeometry(Geometry geo, int[] rgb){
		geoVertices = geo.getVertices();

		for(int[] f: geo.getFace()){
			if(oneFaceVertices == null || oneFaceVertices.length != f.length){
				oneFaceVertices = new double[f.length][6];
				dst = new double[f.length][6];
			}
			
			for(int i=0;i<f.length;i++){
				oneFaceVertices[i] = geoVertices[f[i]];
				
//				geo.matrix.transform(oneFaceVertices[i], dst[i]);
				
				dst[i] = oneFaceVertices[i];
				
//				geo.refreshSurfaceNormal();
			}
			drawPolygon(dst, new int[] {255,0,0});
					
		}

	}
	
	/**
	 * 
	 * @param vertices[#vertices per face][6 (x,y,z,nx,ny,nz)] 
	 * @param rgb
	 */
	public void drawPolygon(double vertices[][], int rgb[]){
		for(int i=1;i<vertices.length-1;i++){
			drawTrangle(vertices[0],vertices[i],vertices[i+1],rgb);
		}
	}
	
	
	private double[][] vtmp = new double[3][6]; 
	private void drawTrangle(double[] a, double[] b, double[] c, int[] rgb){
		vtmp[0] = a;
		vtmp[1] = b;
		vtmp[2] = c;
//		for(double d:vtmp[1]){
//			System.out.print(d+" ");
//		}
//		System.out.println();
		drawTrangle(vtmp, rgb);
	}
	
	private double epsilon = 1;
	private double tmp[];
	private double v[][] = new double[3][7];	//[0]-[5]: x,y,z,r,g,b,zp
	private double area;
//	private double[] d= new double[2];
	private double[] d= new double[7];	//[0]-[5]: x,y,z,r,g,b,zp
	private double tt = 0;
	private double XL = 0;
	private double XR = 0;
	private double XLT = 0;
	private double XRT = 0;
	private double XLB = 0;
	private double XRB = 0;
	private double YT = 0;
	private double YB = 0;
	private int[] rgb_BLACK = {0,0,0};
	private double[] zBuffer = new double[3];
	private int[] rgbTmp = new int[3];
	
	private double[] TLRgb = new double[3];
	private double[] TRRgb = new double[3];
	private double[] BLRgb = new double[3];
	private double[] BRRgb = new double[3];
	private double	TLzp = 0;
	private double	TRzp = 0;
	private double	BLzp = 0;
	private double	BRzp = 0;
	
	private double tx = 0;
	
	private double ZL = 0, ZR=0;
	
	private double[] rgbL = new double[3];
	private double[] rgbR = new double[3];
	
	private double[] dirE = {0,0,1};
	
	/**
	 * 
	 * @param vertices[3][6 (x,y,z,nx,ny,nz)] 
	 * @param rgb
	 */
	private void drawTrangle(double vertics[][], int rgb[]){
		area = 0;
		area += (vertics[0][0] - vertics[1][0]) * (vertics[1][1]+vertics[0][1]) / 2;
		area += (vertics[1][0] - vertics[2][0]) * (vertics[2][1]+vertics[1][1]) / 2;
		area += (vertics[2][0] - vertics[0][0]) * (vertics[0][1]+vertics[2][1]) / 2;

		if(area > 0){
			return;
		}
		
		for(int i=0;i<3;i++){
			v[i] = Arrays.copyOf(vertics[i], 7); 
//			this.mapNomalToRgb(v[i][3], v[i][4], v[i][5], rgbTmp);
//			this.mapNomalToRgb(Arrays.copyOfRange(v[i], 3, 6), rgbTmp);
			
			this.mapNomalToRgb(Arrays.copyOfRange(v[i], 3, 6), cube.material,dirE , rgbTmp);
			
			v[i][3] = rgbTmp[0];	// r
			v[i][4] = rgbTmp[1];	// g
			v[i][5] = rgbTmp[2];	// b
			v[i][6] = FOCAL * v[i][2] / (FOCAL - v[i][2]);	// zp
			zBuffer[i] = -FOCAL;
		}
		
//		for(int i=0;i<7;i++){
//			System.out.print(v[0][i]+" ");
//		}
//		System.out.println();
		
		for(int i=0;i<3;i++)
			for(int j=i;j<3;j++){
				if (v[i][1] > v[j][1]){
					tmp = v[j];
					v[j] = v[i];
					v[i] = tmp;
				}
			}

		//d[]	[0]-[5]: x,y,z,r,g,b,zp
		if(v[2][1] - v[0][1] > epsilon){
			tt = (v[1][1] - v[0][1]) / (v[2][1] - v[0][1]);
			d[0] = v[0][0] + tt*(v[2][0]-v[0][0]);
			d[1] = v[1][1];		//dy;
			d[2] = v[0][2] + tt*(v[2][2]-v[0][2]);		//dz;
			
			d[3] = v[0][3] + tt*(v[2][3]-v[0][3]);	//dr;
			d[4] = v[0][4] + tt*(v[2][4]-v[0][4]);	//dg;
			d[5] = v[0][5] + tt*(v[2][5]-v[0][5]);	//db;
			d[6] = v[0][6] + tt*(v[2][6]-v[0][6]);	//dzp;
					
		}else{
			d[0] = v[2][0];
			d[1] = v[2][1];
			d[2] = v[2][2];		//dz; 
			d[3] = v[2][3];	//dr;
			d[4] = v[2][4];	//dg;
			d[5] = v[2][5];	//db;
			d[6] = v[2][6];				//dzp;
		}
		
		
		
		// upper trangle.
		YT = v[0][1];
		YB = d[1];
		XLT =XRT = v[0][0];
		for(int i=0;i<3;i++){
			TLRgb[i]=TRRgb[i] = v[0][3+i]; //v[3,4,5]= r,g,b;
		}
		if(d[0]<= v[1][0]){
			XLB = d[0];
			XRB = v[1][0];
			for(int i=0;i<3;i++){
				BLRgb[i] = d[3+i]; //d[3,4,5]= r,g,b;
				BRRgb[i] = v[1][3+i]; //v[3,4,5]= r,g,b;
				BLzp = d[6];
				BRzp = v[1][6];
			}
					
		}else{
			XLB = v[1][0];
			XRB = d[0];
			for(int i=0;i<3;i++){
				BLRgb[i] = v[1][3+i]; //d[3,4,5]= r,g,b;
				BRRgb[i] = d[3+i]; 	//v[3,4,5]= r,g,b;
				BLzp = v[1][6];
				BRzp = d[6];
			}
		}
		
		for(int y = (int)Math.ceil(YT); y< YB; y++){
			tt = (y-YT) / (YB-YT);
			XL = XLT + tt* (XLB-XLT);
			XR = XRT + tt* (XRB-XRT);
			ZL = TLzp + tt* (BLzp - TLzp);
			ZR = TRzp + tt* (BRzp - TRzp);
			
			for(int i=0;i<3;i++){
				rgbL[i] =  (int) (TLRgb[i] + tt*(BLRgb[i]-TLRgb[i]));
				rgbR[i] =  (int) (TRRgb[i] + tt*(BRRgb[i]-TRRgb[i]));
			}
			
			for(int x = (int)Math.ceil(XL); x< XR; x++){
				tx = (x-XL) / (XR-XL);
				for(int i=0;i<3;i++){
					rgbTmp[i] = (int) ( rgbL[i]+ tx* (rgbR[i]-rgbL[i]) );  
				}
				setPixel(x,y,rgbTmp);
			}
//			if(XR- XL >200)
//				System.out.println(XL+" "+XR);
		}
		

		
		//lower trangle
		YB = v[2][1];
		YT = d[1];
		for(int i=0;i<3;i++){
			BLRgb[i]=BRRgb[i] = v[2][3+i]; //v[][3,4,5]= r,g,b;
		}
		if(d[0]<= v[1][0]){
			XLT = d[0];
			XRT = v[1][0];
			
			for(int i=0;i<3;i++){
//				BLRgb[i] = d[3+i]; //d[3,4,5]= r,g,b;
//				BRRgb[i] = v[1][3+i]; //v[3,4,5]= r,g,b;
//				BLzp = d[6];
//				BRzp = v[1][6];
				
				TLRgb[i] = d[3+i]; //d[3,4,5]= r,g,b;
				TRRgb[i] = v[1][3+i]; //v[3,4,5]= r,g,b;
				TLzp = d[6];
				TRzp = v[1][6];
			}
		}else{
			XLT = v[1][0];
			XRT = d[0];
			
			for(int i=0;i<3;i++){
//				BLRgb[i] = v[1][3+i]; //d[3,4,5]= r,g,b;
//				BRRgb[i] = d[3+i]; 	//v[3,4,5]= r,g,b;
//				BLzp = v[1][6];
//				BRzp = d[6];
				
				TLRgb[i] = d[3+i]; //d[3,4,5]= r,g,b;
				TRRgb[i] = v[1][3+i]; //v[3,4,5]= r,g,b;
				TLzp = d[6];
				TRzp = v[1][6];
			}
		}
		XLB = XRB = v[2][0];
		for(int y = (int)Math.ceil(YT); y< YB; y++){
			tt = (y-YT) / (YB-YT);
			XL = XLT + tt* (XLB-XLT);
			XR = XRT + tt* (XRB-XRT);
			ZL = TLzp + tt* (BLzp - TLzp);
			ZR = TRzp + tt* (BRzp - TRzp);
			
			for(int i=0;i<3;i++){
				rgbL[i] =  (int) (TLRgb[i] + tt*(BLRgb[i]-TLRgb[i]));
				rgbR[i] =  (int) (TRRgb[i] + tt*(BRRgb[i]-TRRgb[i]));
			}
			
			for(int x = (int)Math.ceil(XL); x< XR; x++){
				tx = (x-XL) / (XR-XL);
				for(int i=0;i<3;i++){
					rgbTmp[i] = (int) ( rgbL[i]+ tx* (rgbR[i]-rgbL[i]) );  
				}
				setPixel(x,y,rgbTmp);
			}
//			if(XR- XL >200)
//				System.out.println(XL+" "+XR);
		}	
	}
	
	
	
	double square(double a){
		return a*a;
	}
	
	private void mapNomalToRgb(double nx, double ny, double nz, int[] rgb){
		rgb[0] =(int) (0+ (nx+1)/2 * 255);
		rgb[1] =(int) (0+ (ny+1)/2 * 255);
		rgb[2] =(int) (0+ (nz+1)/2 * 255);
	}
	private void mapNomalToRgb(double[] N, int[] rgb){
		rgb[0] =(int) (0+ (N[0]+1)/2 * 255);
		rgb[1] =(int) (0+ (N[1]+1)/2 * 255);
		rgb[2] =(int) (0+ (N[2]+1)/2 * 255);
	}
	
	
	double tmp1 = 0;
	double tmp2 = 0;
	double[] Reflection = new double[3];
	double[] tmpRGB = new double[3];
	double[] Ldr;
	private void mapNomalToRgb(double[] N, Material material, double[] dirE, int[] rgb){
		for(int i=0;i<rgb.length;i++){
			tmpRGB[i] = 0;
		}
		
		double[] Argb = material.getAmbientColor();
		double[] Drgb = material.getDiffuseColor();
		double[] Srgb = material.getSpecularColor();
		double p = material.getSpecularPower();
		
		
		
		//light[1] = Lcolor, light[0] = Ldir
		for(double[][] light : this.lights){
			Ldr = light[0].clone();
			normalize(Ldr, Ldr);
//			tmp1 = 2 * dotProduct(light[0], N);
			tmp1 = 2 * dotProduct(Ldr, N);
			for(int i=0;i<3;i++){
//				Reflection[i] = tmp1*N[i] - light[0][i];
				Reflection[i] = tmp1*N[i] - Ldr[i];
			}
//			tmp1 = Math.max(0, dotProduct(light[0],N) );
			tmp1 = Math.max(0, dotProduct(Ldr,N) );
			tmp2 = Math.pow(Math.max(0, dotProduct(Reflection, dirE)), p );
			
			for(int i=0;i<3;i++){
				tmpRGB[i] += light[1][i]*( Drgb[i] * tmp1 + Srgb[i] * tmp2);
			}
			
		}
		
		for(int i=0;i<3;i++){
			tmpRGB[i] += Argb[i];
		}
		gammaCorrection(tmpRGB,rgb,0.45);
	}
	
	private double dotProduct(double[] a, double[] b){
		double rtn = 0;
		for(int i=0;i<a.length && i< b.length;i++){
			rtn += a[i]* b[i];
		}
		return rtn;
	}

	double tmpPoweredSum = 0;
	private void normalize(double[] src, double[] dst){
		tmpPoweredSum = 0;
		for(int i=0;i<src.length;i++){
			tmpPoweredSum += src[i]*src[i];
		}
		tmpPoweredSum = Math.sqrt(tmpPoweredSum);
		for(int i=0;i<src.length && i< dst.length;i++){
			dst[i] = src[i] / tmpPoweredSum;
		}
	}
	private void gammaCorrection(double[] src, int[] dst, double arg){
		for(int i=0;i<3;i++){
			dst[i] = (int) (255* Math.pow(src[i], arg) );
		}
	}


}
