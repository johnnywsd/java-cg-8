
public class Material {
	double[] ambientColor = new double[3];
	double[] diffuseColor = new double[3];
	double[] specularColor = new double[3];
	double specularPower = 0;
	
	public double[] getAmbientColor() {
		return ambientColor;
	}
	public void setAmbientColor(double[] ambientColor) {
		this.ambientColor = ambientColor;
	}
	public double[] getDiffuseColor() {
		return diffuseColor;
	}
	public void setDiffuseColor(double[] diffuseColor) {
		this.diffuseColor = diffuseColor;
	}
	public double[] getSpecularColor() {
		return specularColor;
	}
	public void setSpecularColor(double[] specularColor) {
		this.specularColor = specularColor;
	}
	public double getSpecularPower() {
		return specularPower;
	}
	public void setSpecularPower(double specularPower) {
		this.specularPower = specularPower;
	}
	
}
